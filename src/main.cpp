/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //


#include <talyfem/talyfem.h>
#include <math.h>
#include <fstream>
#include <string>
#include <iostream>
using namespace std;

#include <PFCInputData.h>
#include <PFCNodeData.h>
#include <PFCGridField.h>
#include <PFCEquation.h>

using namespace TALYFEMLIB;
static char help[] = "Solves a Phase-Field Crystal Equation problem!";

inline bool SetIC(PFCGridField &data, PFCInputData &idata) {
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  switch (idata.typeOfIC) {
    case 1:data.SetIC(idata.nsd);
      return true;
      break;
    case 0:
      try {
        load_gf(&data, &idata);
      } catch (const TALYException &e) {
        e.print();
        PrintWarning("Failed to load GridField data!");
        return false;
      }

      data.UpdateDataStructures();
      return true;
      break;
    default:if (rank == 0) std::cerr << "IC not set up " << std::endl;
      return false;
  }
  return false;
}

int main(int argc, char **args) {
  PetscInitialize(&argc, &args, (char *) 0, help);
  try {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    PFCInputData inputData;
    GRID *pGrid = NULL;
    {
      PFCGridField data;

      if ((argc == 2) && (strcmp(args[1], "--printParameters") == 0)) {
        if (rank == 0) { inputData.printAll(std::cout); }
        PetscFinalize();
        return -1;
      }

      // Read input data from file "config.txt"
      inputData.ReadFromFile();

      if (rank == 0) std::clog << inputData;

//  Check if inputdata is complete

      if (!inputData.CheckInputData()) {
        throw (std::string(
            "[ERR] Problem with input data, check the config file!"));
      }
      // Create Grid based on inputdata options
      CreateGrid(pGrid, &inputData);

      // Construct gridfield based on Grid

      data.redimGrid(pGrid);
      data.redimNodeData();

      // Set Initial Conditions
      if (!SetIC(data, inputData)) {
        PrintResults("Failed to set initial conditions", false,
                     inputData.shouldFail);
        delete pGrid;
        throw (std::string("Problem with IC, not loaded"));
      }

      // Set Solver parameters
      int nOfDofPerNode = 3;  // number of degree of freedom per node
      bool do_accelerate = inputData.use_elemental_assembly_;
      AssemblyMethod assembly_method = kAssembleGaussPoints;
      if (inputData.use_elemental_assembly_) {
        assembly_method = kAssembleElements;
      }

      PFCEquation heatEq(&inputData, do_accelerate,
                         assembly_method);  // the equation solver

      heatEq.redimSolver(pGrid, nOfDofPerNode, false, inputData.basisRelativeOrder);
      heatEq.setData(&data);
      PrintStatus("solver initialized!", rank);

      double dt = inputData.dt_;  // time step for time stepping solver
      double t = 0;
      save_gf(&data, &inputData, "data0.plt",t); // Save initial data as a .plt file
      int n_time_steps = inputData.n_time_steps_;  // number of time steps
      // perform solving loop
      double energies[n_time_steps]; // declare and initialize an array for energy values
      for(int i=0;i<n_time_steps;i++){
        energies[i]=0;
      }
      ofstream energyFile;
      energyFile.open("Energy.txt",std::fstream::app); // create a .txt file to save energy for each time step

      for (int i = 0; i < n_time_steps; i++) {
        t += dt;  // update time to new value
        PrintInfo("Solving timestep ", i, "...");
        heatEq.Solve(dt, t);  // solve the equation


        energies[i] = data.CalcEnergy(&inputData); // calculate energy at the corresponding time step
        energyFile <<i*dt<<" "<<energies[i]<<std::endl; // save the time followed by a space then the corresponding energy value

        PrintInfo("   Updating data structures...");
        data.UpdateDataStructures();  // update data for the next cycle



        // Save data plots every ten time steps
        if(i%10==0){
          std::string name = "data";
          name += std::to_string(i) + ".plt";
          save_gf(&data, &inputData, (char *) name.c_str(), t);
        }


      }

      energyFile.close(); // close the Energy.txt file after finishing time stepping

      PrintStatus("Time stepping completed.");

    }
    // clean
    DestroyGrid(pGrid);
  }
    // Finalize PETSC solver
  catch (const std::string &s) {
    PetscFinalize();
    std::cerr << s << std::endl;
    return -1;
  }
  catch (std::bad_alloc e) {
    PetscFinalize();
    std::cerr << "Problem with memory allocation " << e.what();
    return -1;
  }
  catch (const TALYException &e) {
    e.print();
    PetscFinalize();
    return -1;
  }
  catch (...) {
    std::cerr << "Unknown error" << std::endl;
    PetscFinalize();
    return -1;
  }

  PetscFinalize();
  return 0;
}



