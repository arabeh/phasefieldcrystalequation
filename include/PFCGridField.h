/*
  Copyright 2014-2017 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#ifndef PFC_GRIDFIELD_HPP
#define PFC_GRIDFIELD_HPP

#include <iostream>
#include <fstream>
#include "PFCNodeData.h"
#include "PFCInputData.h"
#include <ctime>

class PFCGridField : public GridField<PFCNodeData> {
 public:
  PFCGridField() {}
  virtual ~PFCGridField() {}
  // parameters initialization
  double phiBar = 0.285;
  double C = 0.446;
  double q = 0.66;
  double D = 1;
  double k = 1;
  double epsilon = 1;
  double g = 0;

  void SetIC(int nsd) {
    srand(time(NULL));

    for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
      PFCNodeData *pData = &(GetNodeData(nodeID));
      // initialize phi, phi_pre, omega, omega_pre, H, and H_pre
      double x = p_grid_->GetCoord(nodeID, 0);
      double y = p_grid_->GetCoord(nodeID, 1);

      pData->phi = phiBar + C * (cos((q / sqrt(3)) * y) * cos(q * x) - 0.5 * cos((2 * q / sqrt(3)) * y));
      pData->phi_pre = pData->phi;
      pData->omega = pData->phi * pData->phi * pData->phi + (D * k * k * k * k - epsilon) * pData->phi
          - g * pData->phi * pData->phi +
          2 * ((D * k * k * ((2 / 3) * C * q * q * (-cos((q / sqrt(3)) * y) * cos(q * x)
              + cos((2 * q / sqrt(3)) * y))))
              + 0.5 * D * (8 / 9) * C * q * q * q * q * (cos((q / sqrt(3)) * y) * cos(q * x)
                  - cos((2 * q / sqrt(3)) * y))); //Calculate gradient by hand
      pData->omega_pre = pData->omega;

      pData->H = D * k * k * pData->phi + 0.5 * D * ((2 / 3) * C * q * q * (-cos((q / sqrt(3)) * y) * cos(q * x)
          + cos((2 * q / sqrt(3)) * y)));

      pData->H_pre = pData->H;
    }
    PrintStatusStream(std::cerr, "", "IC set ");
  }
  /**
    * Calculate and return the energy of the system.
    *
    * The energy in this case is defined as used in the following paper:
    *  "An unconditionally energy-stable method for the phase field crystal equation"
    *
    * @param input_data pointer to the input data object that is used to initialize the GRID
    * @return the energy of the system
    */
  double CalcEnergy(const PFCInputData* input_data) {
    FEMElm fe(p_grid_, BASIS_FIRST_DERIVATIVE | BASIS_POSITION | BASIS_DIMENSION_REDUCTION);
    const int spatial_dims = p_grid_->nsd();
    double energy = 0.0;
    double totalEnergy=0.0;
    double const1;
    double averagePhi;
    double laplacianPhi;
    double gradientSquarePhi;
    ZEROPTV dphi;
    const double n_elements = p_grid_->n_elements();
    for (int elm_id = 0; elm_id < n_elements; elm_id++) {
      fe.refill(elm_id, 0);
      averagePhi = valueFEM(fe, 0);
      const1 = -(epsilon / 2.0) * averagePhi * averagePhi - (g / 3.0) * averagePhi * averagePhi * averagePhi
          + 0.25 * averagePhi * averagePhi * averagePhi * averagePhi;
      gradientSquarePhi = 0.0;
      for (int j = 0; j < spatial_dims ; j++) {
        dphi(j) = valueDerivativeFEM(fe, 0, j);
        gradientSquarePhi += dphi(j) * dphi(j);
      }
      laplacianPhi = (2.0 / D) * (valueFEM(fe, 4) - D * k * k * valueFEM(fe, 0));
      while (fe.next_itg_pt()) {
        const double detJxW = fe.detJxW();

        energy += (const1 + (D / 2.0)
            * (laplacianPhi * laplacianPhi - 2 * k * k * gradientSquarePhi + k * k * k * k * averagePhi * averagePhi))
            * detJxW;
      }
    }
    return energy;

  }

};

#endif
