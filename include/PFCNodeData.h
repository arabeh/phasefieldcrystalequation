/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#ifndef PFC_NODEDATA_HPP
#define PFC_NODEDATA_HPP

class PFCNodeData : public NODEData {
 public:

  double phi;
  double phi_pre;
  double omega;
  double omega_pre;
  double H;
  double H_pre;
  double &value(int index) {
    if (index == 0) {
      return phi;
    } else if (index == 1) {
      return phi_pre;
    } else if (index == 2) {
      return omega;
    } else if (index == 3) {
      return omega_pre;
    } else if (index == 4) {
      return H;
    }else if (index == 5) {
      return H_pre;
    }else {
      throw TALYException() << "Invalid PFCNodeData index";
    }
  }

  const double &value(int index) const {
    if (index == 0) {
      return phi;
    } else if (index == 1) {
      return phi_pre;
    } else if (index == 2) {
      return omega;
    } else if (index == 3) {
      return omega_pre;
    } else if (index == 4) {
      return H;
    }else if (index == 5) {
      return H_pre;
    }else {
      throw TALYException() << "Invalid PFCNodeData index";
    }
  }

  static char *name(int index) {
    static char str[256];
    if (index == 0) {
      snprintf(str, 256, "phi");
    } else if (index == 1) {
      snprintf(str, 256, "phi_pre");
    } else if (index == 2) {
      snprintf(str, 256, "omega");
    } else if (index == 3) {
      snprintf(str, 256, "omega_pre");
    }else if (index == 4) {
      snprintf(str, 256, "H");
    }else if (index == 5) {
      snprintf(str, 256, "H_pre");
    }
    return str;
  }

  static int valueno() {
    return 6;
  }
  // update phi_pre, omega_pre, and H_pre values after each time step
  void UpdateDataStructures() {
    phi_pre = phi;
    omega_pre = omega;
    H_pre=H;

  }
};

#endif
